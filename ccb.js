// simple toggler function - toggles whatever element follows element with class toggler
// @see http://renaudjoubert.com/en/article/how-add-javascript-drupal-7-jquery
(function($) {
    Drupal.behaviors.custom_toggle = {
      attach: function(context, settings) {
        $(".ccbtoggler", context).click(function(){
          $(this).next().slideToggle("slow");
          return false;
        }).next().hide();
      }
    };
})(jQuery);