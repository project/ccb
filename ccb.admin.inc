<?php

/**
 * @file
 * Handling of CCB settings
 */

/**
 * Defines the credentials form for storing API address, username and password
 * @see system_settings_form()
 */
 
function ccb_credentials_form() {
  $form['ccb_API_address'] = array(
    '#type' => 'textfield',
    '#title' => 'API URL',
    '#description' => t('Find the API address by going to Settings in CCB (top right) and drilling down to \'API\'. <br />It should be in the form: \'https://{your church name}.ccbchurch.com/api.php\'.'),
    '#default_value' => variable_get('ccb_API_address'),
  );
  $form['ccb_API_username'] = array(
    '#type' => 'textfield',
    '#title' => 'API username',
    '#description' => t('You must create an API user within CCB in order to use the API. When you create a user you also create a username and password.<br /><br />Make sure that you have enabled every service that you want the API user to be able to use (you can select \'All services\' in the right hand column of the user services page in CCB).'),
    '#default_value' => variable_get('ccb_API_username'),
  );
  $form['ccb_API_password'] = array(
    '#type' => 'textfield',
    '#title' => 'API password',
    '#default_value' => variable_get('ccb_API_password'),
  );
  return system_settings_form($form);
}

function ccb_credentials_form_validate($form, &$form_state) {
  $response = call_ccb('individual_profile_from_id', array('describe_api' => 1), '', $form_state['values']['ccb_API_address'], $form_state['values']['ccb_API_username'] . ":" . $form_state['values']['ccb_API_password']);
  if (@$response->response->error) {
    form_set_error('', t('I connected to the CCB API, but it sent the following error message back:<br />') . $response->response->error);
  }
  if (!$response) {
    form_set_error('', t('Unless the CCB server is down, you have entered the API URL wrong. Please check carefully and try again.'));
  }
}

/**
 * Helper function to provide an array of ccb member types
 * @see ccb_settings_form() and ccb_sync_form()
 */
 
function ccb_get_member_types_array() {
  $SimpleXML = call_ccb('membership_type_list');
  $membership_types = array();
  if($SimpleXML) {
    foreach($SimpleXML->response->items->item as $item) {
      $membership_types[(int)$item->id] = $item-> name;
    }
  }
  return $membership_types;
}

function ccb_settings_form() {
  $membership_types = ccb_get_member_types_array();
  $saved_login_types = (is_array(variable_get('ccb_member_types_login'))) ? variable_get('ccb_member_types_login') : array();
  $form['ccb_restrict_login'] = array(
    '#title' => t('Restrict CCB logins to your site by CCB Member type?'),
    '#description' => t('By default anyone who can login to CCB is also able to login to Drupal and is given a user account. If you want to restrict this behaviour to particular CCB Member Types please choose that option here.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('ccb_restrict_login'),
  );
  $form['ccb_member_types_to_login'] = array(
    '#title' => t('Member types which are allowed to log in'),
    '#description' => t('If you\'ve chosen to limit the member types which are allowed to log in, only the membership types which are checked will successfully be able to log into Drupal with their CCB usernames and ids'),
    '#type' => 'checkboxes',
    '#options' => $membership_types,
    '#default_value' => $saved_login_types,
    '#states' => array(
      'visible' => array(
        ':input[name="ccb_restrict_login"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update settings'),
  );
  return $form;
}

function ccb_settings_form_submit($form, &$form_state) {
  $login_types = array();
  foreach($form_state['values']['ccb_member_types_to_login'] as $value) {
    if ($value != 0) $login_types[] = $value;
  }
  variable_set('ccb_restrict_login', $form_state['values']['ccb_restrict_login']);
  variable_set('ccb_member_types_login', $login_types);
  drupal_set_message(t('The CCB settings have been saved.'));
}

function ccb_createUsers_form() {
  $membership_types = ccb_get_member_types_array();
  $saved_sync_types = (is_array(variable_get('ccb_member_types_to_create'))) ? variable_get('ccb_member_types_to_create') : array();
  $form['ccb_create_explanation'] = array(
    '#prefix' => '<p>',
    '#markup' => t('Drupal users which have been created from CCB users (whether through this form, or as just as they log in) will be kept in sync with the CCB records as long as cron is switched on. They will be kept in sync for email addresses, for active/inactive status (will be given a ccb_isactive value for the user in Drupal) and for privacy choice (ccb_isprivate in the Drupal user account). As long as cron jobs are running, new CCB members will have Drupal user accounts created automatically according to the settings on this form. Removing member types to create does not remove existing Drupal accounts, but will prevent new members of that type being automatically created - so please use this form with caution and make sure that you only create the Drupal Users that you want to create.'),
    '#suffix' => '</p>',
  );
  $form['ccb_create'] = array(
    '#title' => t('Create Drupal User Accounts from CCB users'),
    '#description' => t('There is no need to enable this in order to allow CCB users to log on to Drupal. Only enable this if you want to create a Drupal user for every single CCB member, or every single member of a particular member type.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('ccb_create_drupalUsers'),
  );
  $form['ccb_create_restrict'] = array(
    '#title' => t('Choose which CCB Member types to create Drupal accounts for'),
    '#description' => t('By default all CCB users will get a corresponding Drupal user account. Check this box to select only certain member types to create Drupal accounts for.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('ccb_restrict_types_to_create'),
    '#states' => array(
      'visible' => array(
        ':input[name="ccb_create"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['ccb_member_types_to_create'] = array(
    '#title' => t('CCB Member types to create Drupal accounts for'),
    '#description' => t('Warning - if you remove types from this list, existing users that have been created under the previous settings are not deleted from Drupal.'),
    '#type' => 'checkboxes',
    '#options' => $membership_types,
    '#default_value' => $saved_sync_types,
    '#states' => array(
      'visible' => array(
        ':input[name="ccb_create"]' => array('checked' => TRUE),
        ':input[name="ccb_create_restrict"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update settings (and create Drupal user accounts as required).'),
  );
  return $form;
}

function ccb_createUsers_form_submit($form, &$form_state) {
  $sync_types = array();
  foreach($form_state['values']['ccb_member_types_to_create'] as $value) {
    if ($value != 0) $sync_types[] = $value;
  }
  variable_set('ccb_create_drupalUsers', $form_state['values']['ccb_create']);
  variable_set('ccb_restrict_types_to_create', $form_state['values']['ccb_create_restrict']);
  variable_set('ccb_member_types_to_create', $sync_types);
  if (variable_get('ccb_create_drupalUsers') == TRUE) {
    ccb_batch_sync();
  } else {
    drupal_set_message(t('Automatic creation of new Drupal accounts for new CCB members is now switched off. They will only have Drupal accounts created at the point when they log in to Drupal with their CCB credentials.'));
  }
}

/**
 * Set up batch for processing every single record in CCB according to sync requirements
 */

function ccb_batch_sync() {
  $records = array();
  $SimpleXML = call_ccb('valid_individuals');
  
  $batch = array(
    'title' => t('Reading Church Community Builder user records and syncing them with Drupal, creating accounts as required'),
    'operations' => array(),
    'finished' => 'ccb_batch_sync_finished',
    'file' => drupal_get_path('module', 'ccb') . '/ccb.admin.inc', // this file is not in scope unless it is explicitly added here
  );
  foreach ($SimpleXML->valid_individuals->valid_individual as $individual) {
    $ccb_id = (int)$individual['id'];
    $batch['operations'][] = array('ccb_batch_sync_individual', array($ccb_id));
  }
  batch_set($batch);
}

function ccb_batch_sync_individual($ccb_id) {
  $ccb_simpleXML_individual = call_ccb('individual_profile_from_id', array('individual_id' => $ccb_id, 'include_inactive' => 1));
  if ($ccb_individual = $ccb_simpleXML_individual->response->individuals->individual) {
    ccb_sync_individual($ccb_individual);
  }
}

/**
 * Last batch operation to run reports on the success of the batch operation
 */

function ccb_batch_sync_finished($success, $results, $operations) {
  if ($success) {
    variable_set('ccb_last_sync', date('Y-m-d'));
    drupal_set_message(t('Creation of Drupal user accounts complete.')); 
  } else {
    drupal_set_message(t('Please try again, I did not complete successfully.'));
  }
}




