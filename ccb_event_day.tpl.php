<?php

/**
 * @file
 * Default theme implementation for a single day in the list of ccb public
 * calendar events
 *
 * Available variables:
 * - $day : The day of the events
 * - $events : The html for the events during this day.
 *
 * @see ccb_event.tpl.php
 */
?>

<h3><?php print $day; ?></h3>
<?php print $events; ?>
