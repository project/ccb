<?php

/**
 * @file
 * Default theme implementation for a single event in the list of ccb public
 * calendar events
 *
 * Available variables:
 * - $event_name : The name of the event.
 * - $start_time : The time the event starts. Format 'g:ia'
 * - $end_time: The end time of the event.
 * - $location: The published location of the event. Can be empty.
 * - $description: A text description of the event. Can be empty.
 */
?>

<p class="ccbtoggler"><strong><?php print $start_time; ?></strong> <?php print $event_name; ?></p>
<div class="toggled">
<?php 
if($location <> '') print $location . ', ';
print $start_time . ' - ' . $end_time;
if($description <> '') print '<br />' . $description;
?>
</div>